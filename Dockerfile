FROM ruby:3.1.3

WORKDIR /usr/src/app
COPY Gemfile Gemfile.lock ./
RUN bundle install
COPY . .

EXPOSE 3000
CMD ["bundle", "exec", "nanoc", "live", "--host", "0.0.0.0"]
