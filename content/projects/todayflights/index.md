---
title: "TodayFlights"
created_at: Mon Jan 20 09:00:00 +1000 2017
kind: project
licence: MIT
github: https://github.com/josh-/TodayFlights/
download: https://github.com/josh-/TodayFlights/releases/download/v1.1/TodayFlights.dmg
description: TodayFlights is a Notification Center widget that allows you to track flights using the 'FlightUtilities' macOS system framework.
platform: macOS
---

TodayFlights is a Notification Center widget that allows you to monitor flights as they depart, fly and land. It uses the system flight tracking framework included in macOS.

<div class="section group" style="text-align: center; margin-top: 20px;margin-bottom: 20px">
    <div class="col span_1_of_2">
        <a class="fluidbox" href="/images/TodayFlights-v1.1/1.jpg"><img src="/images/TodayFlights-v1.1/1.jpg" style="width: 100%; max-width: 350px; display: block; margin: auto"></a>
    </div>
    <div class="col span_1_of_2">
        <a class="fluidbox" href="/images/TodayFlights-v1.1/2.jpg"><img src="/images/TodayFlights-v1.1/2.jpg" style="width: 100%; max-width: 350px; display: block; margin: auto"></a>
    </div>
</div>

<h3>Usage Instructions</h3>
<p>
    <ol>
        <li>Download the DMG file from above and copy the TodayFlights app to the Applications folder</li>
        <li>Launch the TodayFlights app</li>
        <li>Open <a href="https://support.apple.com/HT204079">Notification Center</a>, select "Today" and click "1 new" at the bottom of the panel</li>
        <li>Add the TodayFlights widget by clicking on the plus icon (+)</li>
    </ol>
</p>

Customise the flight being tracked by hovering over the widget and clicking the info button that appears in the top-right corner.